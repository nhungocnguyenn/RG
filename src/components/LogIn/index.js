import {useState } from "react";
import { Row, Col,Input,Image,Button,message,Typography} from "antd";
import {ArrowRightOutlined } from "@ant-design/icons";
import { withTranslation } from "react-i18next";
import Slide from "react-reveal/Slide";
//import { connect } from "react-redux"
import SvgIcon from "../../common/SvgIcon";
import googleIcon from '../../asset/icons8-google-48.png'
import {serverURL} from '../../_reducer/domain'

import {authByLogin} from "../../_reducer/action/authAction"
import * as S from "./styles";
const emailReg = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
const validation ={
    emailMaxLength:50,
    validationEmail: (input)=>(!emailReg.test(input))? false : true,
}
const {Text} = Typography
const RightBlock = (props) => {
    const {title,content,icon,isRequesting,dispatch} = props
    const [loginEmail,setLoginEmail] = useState("")
    const auth = () => {
        if (!validation.validationEmail(loginEmail)) {
            message.error("Check your input again!")
            return;
        }
        dispatch(authByLogin({email:loginEmail}))
    }
    return (
        <S.RightBlockContainer>
            <Row  justify="space-around" align="middle" id={'intro'}>
                
                <Col lg={10} md={11} sm={11} xs={24} style={{marginLeft:'auto'}}>
                    <Slide left>
                        <S.ContentWrapper>
                            <h6>{title}</h6>
                            <S.Content>{content}</S.Content>
                            <Row style={{marginTop:'31px'}}>
                                <Input value={loginEmail} maxLength={validation.emailMaxLength} style={{width:"375px",height:'35px',border: '1px solid #CAD6E4',borderRadius:'5px',fontSize:'0.875rem'}} onChange ={e=>setLoginEmail(e.target.value)} onPressEnter={()=>auth()} placeholder="Enter your email address"/>
                                <Button className="ml2" onClick={()=>auth()} style={{backgroundColor:"#143083",width:"100px",height:'35px',borderRadius:'5px',fontSize:'0.875rem',lineHeight:'13px',fontWeight:'700',paddingTop:'5px'}} type="primary" disabled={isRequesting} >
                                    Next <ArrowRightOutlined style={{color:"#fff",marginLeft:'10px'}} />
                                </Button>
                            </Row>
                            <div style={{textAlign:"center",width:"375px",marginTop:'30px',color:'#9BABBB',fontSize:'1rem'}}><Text >or</Text></div>
                            <div className="relative" style={{marginTop:'3px'}}>
                                <div className="absolute z-1" style={{width:'35px',height:'35px',backgroundColor:'#fff',paddingLeft:'5px',borderTopLeftRadius:'4px',borderBottomLeftRadius:'4px'}}>
                                    <Image src={googleIcon} width={'25px'} height={'25px'} style={{marginTop:'5px'}} preview={false}/>
                                </div>

                                <Button style={{backgroundColor:'#143083',width:'375px',height:'35px',borderRadius:'4px',fontSize:'0.875rem',fontWeight:'500'}} type='primary' disabled={isRequesting} href={`${serverURL}/auth/google`} loading={isRequesting}> 
                                    Join with your Google Account
                                </Button>
                            
                            </div>
                        </S.ContentWrapper>
                    </Slide>
                </Col>
                <Col lg={13} md={11} sm={12} xs={24}>
                    <Slide right>
                    <SvgIcon
                        src={icon}
                        className="about-block-image"
                        style={{float:'right'}}
                        width="100%"
                        height="100%"
                    />
                    </Slide>
                </Col>
                
            </Row>
        </S.RightBlockContainer>
  );
};

export default withTranslation()(RightBlock);
