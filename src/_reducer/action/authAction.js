import {message} from "antd"
import { request } from "../api"
import {
    FETCH_LOADING,
    FETCH_FINISH,
} from "../constant"
import {interviewURL} from '../domain'
export function authByLogin(data) {
    return dispatch => {
        dispatch({type: FETCH_LOADING})
        request().post("/", data)
        .then(res => {
            dispatch({type: FETCH_FINISH})
            window.location.href = interviewURL + '/signup'
            
        }).catch(() => {
            dispatch({type: FETCH_FINISH})
            message.error("Something wrong")
        })
    }
}


