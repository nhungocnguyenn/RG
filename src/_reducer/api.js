import axios from 'axios'
import {serverURL} from './domain'
export const request = () => {
    
    axios.defaults.withCredentials = true;
    let debugData = (data) => {
        //console.log(data)
        return Promise.resolve(data)
    }
    let debugCatchData = (err) => {
        //console.log(err.message)
        return Promise.reject(err)
    }
    let axiosSource = axios.create({
        headers: {
            // "Authorization": localStorage.getItem("token")
            
        }
    })
    
    return {
        get: async function(url) {
            return await axiosSource.get(serverURL + url).then(debugData).catch(debugCatchData)
        },
        post: async function(url, data) {
            return await axiosSource.post(serverURL + url, data).then(debugData).catch(debugCatchData)
        },
    }
}
