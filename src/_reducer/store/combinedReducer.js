import { combineReducers } from "redux";
import AuthReducer from './authReducer'

const mainReducer = combineReducers({
    AuthReducer
})

export default mainReducer;
