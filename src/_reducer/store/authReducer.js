import {
    FETCH_LOADING,
    FETCH_FINISH,

} from '../constant'

const initState = {
    isRequesting: false,

}

function UserReducer(state = initState, action) {
    switch (action.type) {
        case FETCH_LOADING:
            return {
                ...state,
                isRequesting: true
            }

        case FETCH_FINISH:
            return {
                ...state,
                isRequesting: false
            }
        
        default:
            return state
    }
}

export default UserReducer
