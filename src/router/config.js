const routes = [
  {
    path: ["/", "/home"],
    exact: true,
    component: "Home",
  },
  {
    path:["/about"],
    exact:true,
    component:"About"
  },
  {
    path:["/policy"],
    exact:true,
    component:"Policy"
  },
  {
    path:["/termofservices"],
    exact:true,
    component:"TermOfService"
  }
  
];

export default routes;
