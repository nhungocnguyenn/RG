import { lazy } from "react";
import { connect } from "react-redux"
import IntroContent from "../../content/Home/IntroContent.json";
import MiddleBlockContent from "../../content/Home/MiddleBlockContent.json";
import AboutContent from "../../content/Home/AboutContent.json";
import MissionContent from "../../content/Home/MissionContent.json";
import ProductContent from "../../content/Home/ProductContent.json";
import ContactContent from "../../content/Home/ContactContent.json";

const LogIn = lazy(() => import("../../components/LogIn"));
const ContactFrom = lazy(() => import("../../components/ContactForm"));
const ContentBlock = lazy(() => import("../../components/ContentBlock"));
const MiddleBlock = lazy(() => import("../../components/MiddleBlock"));
const Container = lazy(() => import("../../common/Container"));
const ScrollToTop = lazy(() => import("../../common/ScrollToTop"));

const Home = (props) => {
  return (
    <Container>
      <ScrollToTop />
      <LogIn  icon="developer.svg" id='intro' title={IntroContent.title} content={IntroContent.text} isRequesting={props.isRequesting} dispatch={props.dispatch}/>
      
      <MiddleBlock
        title={MiddleBlockContent.title}
        content={MiddleBlockContent.text}
        button={MiddleBlockContent.button}
      />
      <ContentBlock
        type="left"
        title={AboutContent.title}
        content={AboutContent.text}
        icon="create.svg"
        id="about"
      />
      <ContentBlock
        type="right"
        title={MissionContent.title}
        content={MissionContent.text}
        icon="send.svg"
        id="mission"
      />

      <ContentBlock
        type="left"
        title={ProductContent.title}
        content={ProductContent.text}
        icon="review.svg"
        id="product"
      />
      <ContactFrom
        title={ContactContent.title}
        content={ContactContent.text}
        id="contact"
      />
    </Container>
  );
};
function mapStateToProps(state) {
  const {AuthReducer} = state
  const {isRequesting} = AuthReducer
  return {
      isRequesting
  }
}
export default connect(mapStateToProps)(Home)

