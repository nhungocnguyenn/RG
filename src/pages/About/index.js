import { lazy } from "react";

import MiddleBlockContent from "../../content/About/MiddleBlockContent.json";
import ContactContent from "../../content/Home/ContactContent.json";
const MiddleBlock = lazy(() => import("../../components/MiddleBlock"));
const ContactFrom = lazy(() => import("../../components/ContactForm"));
const Container = lazy(() => import("../../common/Container"));
const ScrollToTop = lazy(() => import("../../common/ScrollToTop"));
const Home = () => {
  return (
    <Container>
      <ScrollToTop />
      <MiddleBlock
        title={MiddleBlockContent.title}
        content={MiddleBlockContent.text}
        id='intro'
      />
      <ContactFrom
        title={ContactContent.title}
        content={ContactContent.text}
        id="contact"
      />
    </Container>
  );
};

export default Home;
