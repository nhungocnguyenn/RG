import { lazy } from "react";

import MiddleBlockContent from "../../content/Policy/MiddleBlockContent.json";
import DataProcessing from "../../content/Policy/DataProcessing.json";
import DataController from "../../content/Policy/DataController.json";
import General from "../../content/Policy/General.json";
import Definition from "../../content/Policy/Definition.json";
import ProcessOfPersonalData from "../../content/Policy/ProcessOfPersonalData.json";
import RightOfData from "../../content/Policy/RightOfData.json";
import SubProcess from "../../content/Policy/SubProcess.json";
import Security from "../../content/Policy/Security.json";
import ReturnandDeletionofCustomerData from "../../content/Policy/ReturnandDeletionofCustomerData.json";
import Termination from "../../content/Policy/Termination.json";
import Liability from "../../content/Policy/Liability.json";
import GoverningLaw from "../../content/Policy/GoverningLaw.json";
import ContactContent from "../../content/Home/ContactContent.json";
const MiddleBlock = lazy(() => import("../../components/MiddleBlock"));
const Container = lazy(() => import("../../common/Container"));
const ContentBlock = lazy(() => import("../../components/ContentBlock"));
const ContactFrom = lazy(() => import("../../components/ContactForm"));
const ScrollToTop = lazy(() => import("../../common/ScrollToTop"));
const Home = () => {
  return (
    <Container>
      <ScrollToTop />
      <MiddleBlock
        title={MiddleBlockContent.title}
        content={MiddleBlockContent.text}
        id={'intro'}
      />
      <MiddleBlock
        title={DataProcessing.title}
        content={DataProcessing.text}
      />
      <MiddleBlock
        title={General.title}
        content={General.text}
      />
      <MiddleBlock
        title={Definition.title}
        content={Definition.text}
      />
      <MiddleBlock
        title={ProcessOfPersonalData.title}
      />
      <MiddleBlock
        title={"3.1 RecruitGenius.ai as a Data Processor."}
        content={["The parties acknowledge and agree that to the extent RecruitGenius.ai operates and manages an recruiting platform and facilitates virtual interview activities on your websites or applications, RecruitGenius.ai is acting as a Data Processor on your behalf, and you act as a Data Controller. RecruitGenius.ai will engage Sub-processors pursuant to the requirements set forth in Section 5 (“Sub-processors”) below."]}
      />
      <MiddleBlock
        title={"3.2 Your Processing of Personal Data. "}
        content={["You shall, in your use of the Services and provision of Instructions, Process Personal Data in accordance with the requirements of Applicable Law and provide Instructions to RecruitGenius.ai that are lawful. You shall ensure that Data Subjects are provided with appropriate information regarding the Processing of their Personal Data and, where required by Applicable Law, you shall obtain their consent to such Processing."]}
      />
      <MiddleBlock
        title={"3.3 RecruitGenius.ai’s Processing of Personal Data. "}
        content={["To the extent that RecruitGenius.ai is acting as a Data Processor, RecruitGenius.ai will:(a) Process Personal Data in accordance with the Instructions of the Data Controller and this DPA; (b) ensure that any person authorized by RecruitGenius.ai to Process Personal Data is committed to respecting the confidentiality of the Personal Data;(c) provide reasonable assistance to the Data Controller, at the expense of the Data Controller, in ensuring compliance with the obligations of the Data Controller under Applicable Laws, taking into account the nature of the Processing and the information available to the Data Processor; (d) contribute to audits or inspections conducted by RecruitGenius.ai’s authorized auditors by making available to the Data Controller upon reasonable request the respective audit reports (no more frequently than once per year) provided that the Data Controller enters into a non-disclosure agreement with RecruitGenius.ai regarding such audit reports; and (e) provide reasonable assistance to the Data Controller, upon request, and, at the expense of the Data Controller, facilitate the Data Controller’s compliance with its obligations in respect of conducting data protection impact assessments and consulting with a supervisory authority, as required by Applicable Law."]}
      />
      <MiddleBlock
        title={"3.4 Details of the Processing. "}
        content={["The subject-matter of Processing of Personal Data by RecruitGenius.ai is the performance of the Services pursuant to the RecruitGenius.ai Agreement. The duration of the Processing, the nature and purpose of the Processing, the types of Personal Data and categories of Data Subjects Processed under this DPA are further specified in Schedule A to this DPA."]}
      />
      <MiddleBlock
        title={RightOfData.title}
      />
      <MiddleBlock
        title={"4.1 Data Subject Requests. "}
        content={["RecruitGenius.ai will, to the extent permitted by Applicable Law or other applicable legal or regulatory requirements, inform you of any formal requests from Data Subjects exercising their rights of access, correction or erasure of their Personal Data, their right to restrict or to object to the Processing as well as their right to data portability, and will not to respond to such requests, unless instructed by you in writing to do so."]}
      />
      <MiddleBlock
        title={"4.2 Assistance by RecruitGenius.ai."}
        content={["RecruitGenius.ai shall, upon your request, provide reasonable efforts to assist you in responding to such Data Subject requests, and to the extent legally permitted, you shall be responsible for any costs arising from RecruitGenius.ai’s provision of such assistance."]}
      />
      <MiddleBlock
        title={SubProcess.title}
      />
      <MiddleBlock
        title={"5.1 Appointment of Sub-Processors."}
        content={["You acknowledge and agree that:(a) RecruitGenius.ai affiliates may be retained as Sub-Processors; and (b) RecruitGenius.ai and RecruitGenius.ai affiliates may engage third-party Sub-Processors in connection with the provision of the Services. RecruitGenius.ai or a RecruitGenius.ai affiliate will enter into a written agreement with the Sub-Processor imposing on the Sub-Processor data protection obligations comparable to those imposed on RecruitGenius.ai under this Agreement with respect to the protection of Personal Data. In case the Sub-Processor fails to fulfill its data protection obligations under such written agreement with RecruitGenius.ai, RecruitGenius.ai will remain liable to you for the performance of the Sub-Processor’s obligations under such agreement, except as otherwise set forth in the RecruitGenius.ai Agreement. By way of this DPA, the Data Controller provides general written authorization to RecruitGenius.ai as Data Processor to engage Sub-Processors as necessary to perform the Services."]}
      />
      <MiddleBlock
        title={"5.2 List of Current Sub-Processors."}
        content={["RecruitGenius.ai shall make available a list of Sub-Processors for the Services. RecruitGenius.ai will update the list to reflect any addition, replacement or other changes to RecruitGenius.ai’s Sub-Processors."]}
      />
      <MiddleBlock
        title={"5.3. Objection Right for New Sub-Processors."}
        content={["You may reasonably object to RecruitGenius.ai’s use of a new Sub-Processor on legitimate grounds, subject to the termination and liability clauses of the RecruitGenius.ai Agreement. The Data Controller acknowledges that these Sub-Processors are essential to providing the Services and that objecting to the use of a Sub-Processor may prevent RecruitGenius.ai from offering the Services to the Data Controller."]}
      />
      <MiddleBlock
        title={Security.title}
      />
      <MiddleBlock
        title={"6.1 Controls for the Protection of Personal Data."}
        content={["Each party shall implement and maintain appropriate technical and organizational measures for protection of the security, confidentiality and integrity of Personal Data, including, where appropriate:(a) Pseudonymization and encryption of Personal Data;(b) the ability to ensure the ongoing confidentiality, integrity, availability and resilience of processing systems and services involved in the processing of Personal Data; (c) the ability to restore the availability and access to Personal Data in a timely manner in the event of a physical or technical incident; and (d) a process for regular testing, assessing and evaluating the effectiveness of technical and organizational measures for ensuring the security of the processing of Personal Data."]}
      />
      <MiddleBlock
        title={"6.2 Personal Data Incident Management and Notification."}
        content={["RecruitGenius.ai will implement and maintain a data security incident management program, compliant with Applicable Law, that addresses management of data security incidents including a loss, theft, misuse, unauthorized access, disclosure, or acquisition, destruction or other compromise of Personal Data (“Incident”). Except to the extent necessary to comply with applicable legal, regulatory or law enforcement requirements, RecruitGenius.ai will inform you without unreasonable delay in accordance with Applicable Law after it becomes aware of any Incident that has occurred in its systems which affects Personal Data RecruitGenius.ai processes on your behalf."]}
      />
      <MiddleBlock
        title={ReturnandDeletionofCustomerData.title}
        content={ReturnandDeletionofCustomerData.text}
      />
      <MiddleBlock
        title={DataController.title}
        content={DataController.text}
      />
            
      <MiddleBlock
        title={Termination.title}
        content={Termination.text}
      />
      <MiddleBlock
        title={Liability.title}
        content={Liability.text}
      />
      <MiddleBlock
        title={GoverningLaw.title}
        content={GoverningLaw.text}
      />
      <ContactFrom
        title={ContactContent.title}
        content={ContactContent.text}
        id="contact"
      />
      
    </Container>
  );
};

export default Home;
