const SvgIcon = ({ src, width, height,style }) => (
  <img src={`/img/svg/${src}`} style={style} alt={src} with={width} height={height} />
);

export default SvgIcon;
